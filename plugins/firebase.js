export default (context, inject) => {
  const config = {
    apiKey: context.env.FIREBASE_API_KEY,
    authDomain: context.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: context.env.FIREBASE_DATABASE_URL,
    projectId: context.env.FIREBASE_PROJECT_ID,
    storageBucket: context.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: context.env.FIREBASE_MESSAGING_SENDER_ID,
    appId: context.env.FIREBASE_APP_ID
  }
  !firebase.apps.length ? firebase.initializeApp(config) : firebase.app()
  inject('firebase', firebase)
  inject('firebaseui', new firebaseui.auth.AuthUI(firebase.auth()))
}
