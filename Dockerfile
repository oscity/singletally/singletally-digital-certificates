FROM node:10

WORKDIR /usr/src/app

COPY . .

RUN yarn install \
  --prefer-offline \
  --frozen-lockfile \
  --non-interactive \
  --production=false

RUN yarn build

# Run the web service on container startup.
CMD [ "yarn", "start" ]
