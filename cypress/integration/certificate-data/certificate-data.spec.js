describe('Cerificate data', function() {
  before('Login', function() {
    cy.login()
  })

  after('Logout', function() {
    cy.logout()
  })

  beforeEach('Visti certificate data', function() {
    cy.visit('/certificate-data')
    // eslint-disable-next-line
    cy.wait(1000)
  })

  it('Logged user', function() {
    cy.get('[data-cy=button_login_user]').as('button_login_user')
    cy.get('@button_login_user').should('be.visible')
    cy.get('@button_login_user').contains('Cerrar sesión')
  })

  it('Render certificate component', function() {
    cy.get('[data-cy=data_table_certificate]').should('be.visible')
  })

  it('Redirect to load data', function() {
    cy.get('[data-cy=upload_new_data]').as('upload_new_data')
    cy.get('@upload_new_data').should('be.visible')
    cy.get('@upload_new_data').click()
    cy.url().should('to.equal', Cypress.env('baseUrl') + '/load-data')
  })

  it('New cell', function() {
    cy.get('[data-cy=dialog_new_cell]').as('dialog_new_cell')
    cy.get('@dialog_new_cell').should('not.be.visible')
    cy.get('[data-cy=data_table_certificate]').should('be.visible')
    // eslint-disable-next-line
    cy.wait(1000)
    cy.get('table tbody tr')
      .its('length')
      .as('total_rows_in_table')
    cy.get('[data-cy=new_cell]').click()
    cy.get('@dialog_new_cell').should('exist')
    cy.get('[data-cy=titular_new_cell]').type('Titular nueva celda')
    cy.get('[data-cy=mun_new_cell]').type('Municipio nueva celda')
    cy.get('[data-cy=estado_new_cell]').type('Estado nueva celda')
    cy.get('[data-cy=com_new_cell]').type('Comunidad nueva celda')
    cy.get('[data-cy=time_new_cell]').type('Tiempo nueva celda')
    cy.get('[data-cy=tec_new_cell]').type('Técnica nueva celda')
    cy.get('[data-cy=save_new_cell]').click()
    cy.get('@total_rows_in_table').then((oldLength) => {
      if (oldLength !== 1) {
        cy.get('table tbody tr')
          .its('length')
          .should('be.gt', oldLength)
      }
    })
  })

  it('Activate certificate data', function() {
    cy.get('[data-cy=certify]').as('certify')
    cy.get('@certify').should('be.disabled')
    cy.get('[data-cy=tab_without_certificator]').as('tab_without_certificator')
    cy.get('@tab_without_certificator').click()
    // eslint-disable-next-line
    cy.wait(1000)
    cy.selectData()
    cy.get('@certify').should('not.be.disabled')
  })

  it('Select blockchain network', function() {
    cy.get('[data-cy=dialog_blockchain_network]').as('dialog')
    cy.get('[data-cy=dialog_confirm]').as('dialog_confirm')
    cy.get('@dialog').should('not.be.visible')
    cy.get('@dialog_confirm').should('not.be.visible')
    cy.get('[data-cy=tab_without_certificator]').click()
    // eslint-disable-next-line
    cy.wait(1000)
    cy.selectData()
    cy.get('[data-cy=certify]').click()
    cy.get('@dialog').should('not.have.attr', 'display')
    cy.get('[data-cy=bitcoin]').should('be.checked')
    cy.get('[data-cy=acept_newtwork]').click()
    cy.get('@dialog_confirm').should('not.have.attr', 'display')
  })

  it('Check status for certified data', function() {
    cy.get('[data-cy=tab_certificates]').click()
    cy.get('table tbody tr').each(function(element) {
      if (!element.text().includes('Certificado')) {
        element.text().includes('td', 'En espera')
      }
    })
  })
})
