describe('Logout', function() {
  before('Login', function() {
    cy.login()
  })

  it('Logout user', function() {
    cy.logout()
  })
})
