import 'cypress-file-upload'

const fileName = '../fixtures/singletally_test.csv'

Cypress.Commands.add('uploadFile', () => {
  cy.fixture(fileName).then((fileContent) => {
    cy.get('[data-cy="file_pond"]').upload(
      {
        fileContent,
        fileName: 'singletally_test.csv',
        mimeType: 'text/csv',
        encoding: 'utf-8'
      },
      { subjectType: 'drag-n-drop' }
    )
  })
})

Cypress.Commands.add('selectData', () => {
  cy.get('.v-input--selection-controls__ripple')
    .first()
    .as('select_all')
  cy.get('@select_all').click()
})

Cypress.Commands.add('login', () => {
  cy.visit('/')
  cy.get('.firebaseui-idp-list > :nth-child(2)').click()
  cy.get('.firebaseui-id-password').should('not.exist')
  cy.get('.firebaseui-id-email')
    .type(Cypress.env('EMAIL_LOGIN'))
    .should('have.value', Cypress.env('EMAIL_LOGIN'))
  cy.get('.firebaseui-form-actions > :nth-child(2)').click()
  cy.get('.firebaseui-id-password').should('be.visible')
  cy.get('.firebaseui-id-password')
    .type(Cypress.env('PASSWORD_LOGIN'))
    .should('have.value', Cypress.env('PASSWORD_LOGIN'))
  cy.get('.firebaseui-form-actions > :nth-child(1)').click()
  cy.url().should('to.equal', Cypress.env('baseUrl') + '/certificate-data')
})

Cypress.Commands.add('logout', () => {
  cy.get('[data-cy=button_login_user]').as('button_login_user')
  cy.get('@button_login_user').should('be.visible')
  cy.get('@button_login_user').contains('Cerrar sesión')
  cy.get('@button_login_user').click()
  cy.url().should('to.equal', Cypress.env('baseUrl') + '/')
  cy.get('@button_login_user').contains('Iniciar sesión')
})
