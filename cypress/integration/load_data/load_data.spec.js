describe('Load data', function() {
  before('Login', function() {
    cy.login()
  })

  after('Logout', function() {
    cy.logout()
  })

  beforeEach('Visti load data', function() {
    cy.visit('/load-data')
  })

  function loadData() {
    cy.get('[data-cy=dialog_confirm_save_data]').as('dialog')
    cy.get('@dialog').should('not.be.visible')
    cy.selectData()
    cy.get('[data-cy=save_data]').click()
    cy.get('@dialog').should('exist')
    cy.get('[data-cy=button_acept]').click()
    cy.url().should('to.equal', Cypress.env('baseUrl') + '/certificate-data')
  }

  it('Render load component', function() {
    cy.get('[data-cy=load_data_buttons]').should('be.visible')
    cy.get('[data-cy=load_data_buttons] > button')
      .its('length')
      .should('eq', 2)
  })

  it('Render load component for paste data', function() {
    cy.get('[data-cy=paste_data]').click()
    cy.get('[data-cy=table_data]').should('not.be.visible')
    cy.get('[data-cy=area_paste_data]').should('be.visible')
  })

  it('Render load component for upload file', function() {
    cy.get('[data-cy=upload_file]').click()
    cy.get('[data-cy=table_data]').should('not.be.visible')
    cy.get('[data-cy=file_pond]').should('be.visible')
  })

  it('Logged user', function() {
    cy.get('[data-cy=button_login_user]').as('button_login_user')
    cy.get('@button_login_user').should('be.visible')
    cy.get('@button_login_user').contains('Cerrar sesión')
  })

  it('Paste data', function() {
    cy.get('[data-cy=paste_data]').click()
    cy.get('[data-cy=table_data]').should('not.be.visible')
    cy.get('[data-cy=area_paste_data]').should('be.visible')
    cy.get('[data-cy=area_paste_data]').type(
      'titular	estado_origen	mun_origen	com_origen	time	tec_art\nPrueba paste	Estado Prueba paste	Municipio prueba paste	Comunidad prueba paste	Tiempo prueba paste	Técnica prueba paste'
    )
    cy.get('[data-cy=title_load_data]').click()
    // eslint-disable-next-line
    cy.wait(1000)
    cy.get('[data-cy=table_data]').should('be.visible')
    cy.get('[data-cy=area_paste_data]').should('not.be.visible')
    loadData()
  })

  it('Upload file', function() {
    cy.get('[data-cy=upload_file]').click()
    cy.get('[data-cy=table_data]').should('not.be.visible')
    cy.get('[data-cy=file_pond]').should('be.visible')
    cy.uploadFile()
    // eslint-disable-next-line
    cy.wait(1000)
    cy.get('[data-cy=button_upload_file]').as('upload_file')
    cy.get('@upload_file').click()
    // eslint-disable-next-line
    cy.wait(1000)
    cy.get('[data-cy=table_data]').should('be.visible')
    cy.get('[data-cy=file_pond]').should('not.be.visible')
    loadData()
  })
})
