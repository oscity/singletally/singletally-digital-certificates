# Singletally Digital Certificates

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code-of-conduct.md)

Singletally Digital Certificates is the blockchain platform that allows you to leapfrog digitalization and go straight to use the most secure, transparent, portable, and interoperable digital certificates using **blockchain**. It is an open source project funded by [Unicef](https://www.unicef.org/) and powered by ⚡️ [OS City](https://os.city/) to encourage people to contribute to open standards projects related to blockchain technology.

This application allows users to easily select a desired blockchain, upload a roster with the information to be certified and then implement the [Blockcerts](https://www.blockcerts.org/) Open Standards to create a blockchain digital certificate that is tamper-proof and openly verifiable by anyone. We've used this repository to write into multiple blockchains including Bitcoin, Ethereum, Ethereum Classic, RSK, and even the BFA (Federal Argentinian Blockchain); currently working in open sourcing an easy way for developers to edit which blockchain to write to.



## Documentation

This project needs to be built in order to be used. To install this project please refer to: [Installation Page](https://gitlab.com/oscity/singletally/singletally-digital-certificates/wikis/Installing)

Also take a look at our [Wiki](https://gitlab.com/oscity/singletally/singletally-digital-certificates/wikis/), there you can find the whole documentation.

Don't forget you can improve our Wiki by contribuiting with awesome suggestions.

## Built With

The following technologies helped us build this application 👌

|                             Logo                             |             Dependency name              |             Description             |
| :----------------------------------------------------------: | :--------------------------------------: | :---------------------------------: |
| <img width=50 src="https://sendgrid.com/wp-content/themes/sgdotcom/pages/resource/brand/2016/SendGrid-Logomark.png"> |    [SendGrid](https://sendgrid.com/)     |             Sends email             |
| <img width=50 src="https://cdn4.iconfinder.com/data/icons/google-i-o-2016/512/google_firebase-2-512.png"> | [Firebase](https://firebase.google.com/) | Authentication, Firestore & Storage |
| <img width=50 src="https://cdn.iconscout.com/icon/free/png-512/vue-282497.png"> |        [Vue](https://vuejs.org/)         |        JavaScript Framework         |
| <img width=50 src="https://dx6hw0azfi3av.cloudfront.net/wp-content/uploads/nuxt-logo.svg"> |       [Nuxt](https://nuxtjs.org/)        |         Server side render          |
| <img width=50 src="https://styles.redditmedia.com/t5_3nu8v/styles/communityIcon_zuqnf4r5ml111.png"> |   [Vuetify](https://vuetifyjs.com/en/)   |       Material Design for Vue       |
| <img width=50 src="https://static.vaadin.com/directory/user11/icon/file1254560279065072985_1554458173229file1312710634264522330_1554291323036leaflet-directory-logo-example.png"> |    [Leaflet](https://leafletjs.com/)     |         A map layer creator         |

## Questions
For questions and support please use the official [forum](https://groups.google.com/d/forum/singletally-digital-certificates). The issue list of this repo is exclusively for bug reports and feature requests.

## Issues
Please make sure to read the [Issue Reporting Checklist](https://gitlab.com/oscity/singletally/singletally-digital-certificates/issues) before opening an issue. Issues not conforming to the guidelines may be closed immediately.

## Contributing

Please read our [Code of Conduct](./CODE_OF_CONDUCT.md) and our [Contribution Guide](./CONTRIBUTING.md) for details about the process for submitting merge requests to us.

## Versioning

See the full list of versions in [tags list](https://gitlab.com/oscity/singletally/singletally-digital-certificates/-/tags)

## Authors

This project is currently developed and maintained by [OS City](https://os.city/).
See also the list of [contributors](https://gitlab.com/oscity/singletally/singletally-digital-certificates/-/graphs/dev) who participated in this project.

## License

This project is licensed under the MIT License - see the [License](./LICENSE.md) file for details