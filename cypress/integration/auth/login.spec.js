describe('Login with email', function() {
  beforeEach('Visti login', function() {
    cy.visit('/')
  })

  it('Render login component', function() {
    cy.get('.firebaseui-idp-list > li').as('list_login')
    cy.get('@list_login').should('be.visible')
    cy.get('@list_login')
      .its('length')
      .should('eq', 2)
  })

  it('Unlogged user', function() {
    cy.get('[data-cy=button_login_user]').as('button_login_user')
    cy.get('@button_login_user').should('be.visible')
    cy.get('@button_login_user').contains('Iniciar sesión')
  })

  it('Login with email and password correct', function() {
    cy.login()
    cy.logout()
  })

  it('Login with email correct and password incorrect', function() {
    cy.get('.firebaseui-idp-list > :nth-child(2)').click()
    cy.get('.firebaseui-id-password').should('not.exist')
    cy.get('.firebaseui-id-email')
      .type(Cypress.env('email'))
      .should('have.value', Cypress.env('email'))
    cy.get('.firebaseui-form-actions > :nth-child(2)').click()
    cy.get('.firebaseui-id-password').should('be.visible')
    cy.get('.firebaseui-id-password')
      .type('passwordincorrect')
      .should('have.value', 'passwordincorrect')
    cy.get('.firebaseui-error-wrapper').should('not.have.value')
    cy.get('.firebaseui-form-actions > :nth-child(1)').click()
    cy.get('.firebaseui-error-wrapper').contains(
      'El correo electrónico y la contraseña que ingresaste no coinciden'
    )
  })

  it('Login with new email', function() {
    cy.get('.firebaseui-idp-list > :nth-child(2)').click()
    cy.get('.firebaseui-id-name').should('not.exist')
    cy.get('.firebaseui-id-new-password').should('not.exist')
    cy.get('.firebaseui-id-email').type(generateEmail())
    cy.get('.firebaseui-form-actions > :nth-child(2)').click()
    cy.get('.firebaseui-id-name').should('be.visible')
    cy.get('.firebaseui-id-new-password').should('be.visible')
    cy.get('.firebaseui-id-name')
      .type('Pamela Test')
      .should('have.value', 'Pamela Test')
    cy.get('.firebaseui-id-new-password')
      .type('holiholi')
      .should('have.value', 'holiholi')
    cy.get('.firebaseui-error-wrapper').should('not.have.value')
    cy.get('.firebaseui-form-actions > :nth-child(2)').click()
    cy.url().should('to.equal', Cypress.env('baseUrl') + '/certificate-data')
    cy.logout()
  })

  function generateEmail() {
    const variableEmail = Math.floor(Math.random() * 10000) + 1
    return `pamela+${variableEmail}@os.city`
  }
})
