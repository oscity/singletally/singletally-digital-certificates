const functions = require('firebase-functions')
const sgMail = require('@sendgrid/mail')
const cors = require('cors')({ origin: true })

exports.emailMessage = functions.https.onRequest((req, res) => {
  const { usuario, nameDoc } = req.body
  return cors(req, res, () => {
    /**
     * @todo #5 Use a sendgrid template
     */
    const text = `
    <div>
      <h4>Certificado</h4>
      <p>usuario - ${usuario || ''}</p>
      <p>Documento - ${nameDoc || ''}</p>
    </div>`
    const msg = {
      to: '<destination-email@domain>',
      cc: '<this line is optional, could send a cc or not>',
      from: process.env.NOREPLY_EMAIL,
      subject: `<Subject>`,
      text,
      html: text
    }
    sgMail.setApiKey(process.env.SENDGRID_API_KEY)
    sgMail
      .send(msg)
      .then((response) => {
        return res.status(200).send('success')
      })
      .catch((error) => {
        console.log(error)
        return res.status(404).send('fail')
      })
  })
})
