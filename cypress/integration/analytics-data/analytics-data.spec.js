describe('Analytics data', function() {
  before('Login', function() {
    cy.login()
  })

  after('Logout', function() {
    cy.logout()
  })

  beforeEach('Visti analytics data', function() {
    cy.visit('/analytics-data')
    // eslint-disable-next-line
    cy.wait(1000)
  })

  it('Logged user', function() {
    cy.get('[data-cy=button_login_user]').as('button_login_user')
    cy.get('@button_login_user').should('be.visible')
    cy.get('@button_login_user').contains('Cerrar sesión')
  })

  it('Render certificate component', function() {
    cy.get('[data-cy=map-location]').should('be.visible')
  })

  it('Zoom in map', function() {
    cy.get('.leaflet-control-zoom > a').as('content_controls')
    cy.get('@content_controls').should('be.visible')
    cy.get('@content_controls')
      .its('length')
      .should('eq', 2)
  })

  it('Show location data', function() {
    cy.get('.leaflet-popup').should('not.exist')
    cy.get('.leaflet-pane .leaflet-marker-pane img')
      .first()
      .as('marker')
    cy.get('@marker').click({ force: true })
    cy.get('.leaflet-popup').should('exist')
  })
})
